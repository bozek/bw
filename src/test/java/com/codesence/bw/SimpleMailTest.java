package com.codesence.bw;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;

import com.codesence.bw.config.MailConfig;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;


/**
 * Test direct mail sending using JavaMailSenderImpl configured in MailConfig.java.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MailConfig.class)
public class SimpleMailTest {

	@Resource(name = "simpleMailSender")
	private MailSender mailSender;

	private GreenMail server;

	@Before
	public void init() {
		server = new GreenMail(ServerSetupTest.SMTP);
		server.start();
	}

	@Test
	public void testEmail() throws InterruptedException, MessagingException {
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom("test_from@example.com");
		message.setTo("test_to@example.com");
		message.setSubject("test subject");
		message.setText("test message");
		mailSender.send(message);

		server.waitForIncomingEmail(1);

		Message[] messages = server.getReceivedMessages();
		assertEquals(1, messages.length);
		assertEquals("test subject", messages[0].getSubject());
		String body = GreenMailUtil.getBody(messages[0]).replaceAll("=\r?\n", "");
		assertEquals("test message", body);
	}

	@After
	public void cleanup() {
		server.stop();
	}
}