package com.codesence.bw.common;

/**
 * @author Ladislav Bozek
 */
public class UrlSettings {
    public static class App {
        public static final String URL = "/app";
        public static final String URL_AJAX = "/ajax/app";
        public static final String INIT = "/init";
    }

    public static class Location {

        public static final String URL = "/locations";
        public static final String IP = "/ip";
    }

}
