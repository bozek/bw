package com.codesence.bw.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.codesence.bw.model.Location;
import com.codesence.bw.service.LocationService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author Ladislav Bozek
 */
@Controller
public class LocationController {

	@Resource
	LocationService locationService;

	@RequestMapping(value = "/ajax/locations/read/${ip}", method = RequestMethod.GET)
	public @ResponseBody Location getLocationByIp(@PathVariable String ip) {
		Location location = locationService.getLocation(ip.replaceAll("_", "."));
		return location;

	}

	@RequestMapping(value = "/ajax/locations/read", method = RequestMethod.GET)
	public @ResponseBody Location getLocation() {
		Location location = locationService.getLocation("89.173.4.186");
//		Location location = locationService.getLocation("212.89.239.26");
		return location;

	}

	/**
	 * Search location by city name
	 *
	 * @param city
	 * @return
	 */
	@RequestMapping(value = "/ajax/locations/search", method = RequestMethod.GET)
	public @ResponseBody List<String> search(@RequestParam String city) {

		List<String> cities = new ArrayList<>();
		List<String> found = new ArrayList<>();
		cities.add("Bratislava");
		cities.add("Zilina");

		// search city
		for (String item : cities) {
			if (item.contains(city)) {
				found.add(item);
			}
		}

		return found;
	}

}