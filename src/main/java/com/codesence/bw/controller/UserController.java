package com.codesence.bw.controller;

import javax.inject.Inject;

import com.codesence.bw.model.Payment;

import org.springframework.http.MediaType;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

	private final Facebook facebook;

	@Inject
	public UserController(Facebook facebook) {
		this.facebook = facebook;
	}

	@RequestMapping(value = "/ajax/user/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Payment register() {
		Payment payment = new Payment();
		return payment;
	}

	/**
	 * Finish button, when pressed user should be redirected to home page.
	 *
	 * @return
	 */
	@RequestMapping(value = "/ajax/user/contributed", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String contributed() {
		return "OK";
	}
}
