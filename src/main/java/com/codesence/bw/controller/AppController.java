package com.codesence.bw.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.codesence.bw.model.ConfigurationResponse;
import com.codesence.bw.model.Location;
import com.codesence.bw.model.Organization;
import com.codesence.bw.model.User;
import com.codesence.bw.model.Weather;
import com.codesence.bw.model.WeatherOption;
import com.codesence.bw.model.WeatherOptions;
import com.codesence.bw.service.LocationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.codesence.bw.common.UrlSettings.App.INIT;
import static com.codesence.bw.common.UrlSettings.App.URL_AJAX;

@Controller
public class AppController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppController.class);

	private final Facebook facebook;

	@Resource
	private LocationService locationService;

	@Inject
	public AppController(Facebook facebook) {
		this.facebook = facebook;
	}

	@RequestMapping(value = URL_AJAX + INIT, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ConfigurationResponse init(HttpServletRequest request) {
		LOGGER.info("init() method started");

		String ip = request.getRemoteAddr();
		LOGGER.info("IP: {}", ip);


		Set<String> localAddresses = new HashSet<String>();
		try {
			localAddresses.add(InetAddress.getLocalHost().getHostAddress());
			for (InetAddress inetAddress : InetAddress.getAllByName("localhost")) {
				localAddresses.add(inetAddress.getHostAddress());
			}
		} catch (IOException e) {
			LOGGER.warn("Unable to lookup local addresses");
		}

		if (localAddresses.contains(request.getRemoteAddr())) {
			LOGGER.info("Local access detected, changing to real IP");
			ip = "212.89.239.26";
		} else {
			LOGGER.info("Remote access");
		}

		User user = new User();
		Date tomorrow = new Date();
		Location location = locationService.getLocation(ip);
		Weather weather = new Weather(tomorrow, 25);
		WeatherOptions weatherOptions = new WeatherOptions();
		for (int i = -5; i < 5; i++) {
			weatherOptions.add(new WeatherOption(new Weather(tomorrow, 20 - (i * 4)), new BigDecimal(Math.abs(i) * 0.5)));
		}
		Organization organization = new Organization();

//        LOGGER.info("Generating configuration: {} {} {} {} {}", new Object[] {location, weather, user, weatherOptions, organization});
		ConfigurationResponse configuration = new ConfigurationResponse(location, weather, user, weatherOptions, organization);

		return configuration;
	}
}
