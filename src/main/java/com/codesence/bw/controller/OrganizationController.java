package com.codesence.bw.controller;

import com.codesence.bw.model.Organization;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class OrganizationController {

//    private final Facebook facebook;

//    @Inject
    public OrganizationController() {
//        this.facebook = facebook;
    }

    /**
     * Lists all nearby organizations.
     * @return
     */
    @RequestMapping(value = "/ajax/organizations/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Organization> list() {
        List<Organization> list = new ArrayList<>();
		list.add(new Organization("Org1"));
        list.add(new Organization("Org2"));
        return list;
    }
}
