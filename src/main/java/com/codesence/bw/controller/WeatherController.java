package com.codesence.bw.controller;

import java.util.Date;

import javax.inject.Inject;

import com.codesence.bw.model.Weather;

import org.springframework.http.MediaType;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WeatherController {

	private final Facebook facebook;

	@Inject
	public WeatherController(Facebook facebook) {
		this.facebook = facebook;
	}

	@RequestMapping(value = "/ajax/weather/tomorrow", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Weather home() {
		Weather tomorrow = new Weather(new Date(), 25);
		return tomorrow;
	}
}
