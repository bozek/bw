package com.codesence.bw.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

	private final Facebook facebook;

	@Inject
	public HomeController(Facebook facebook) {
		this.facebook = facebook;
	}

	@RequestMapping(value = "/facebook", method = RequestMethod.GET)
	public String facebook(Model model) {
		List<Reference> friends = facebook.friendOperations().getFriends();
		List<String> friendIds = facebook.friendOperations().getFriendIds();


		FacebookProfile profile = facebook.userOperations().getUserProfile();

		model.addAttribute("friends", friends);
		model.addAttribute("friendIds", friendIds);
		return "facebook";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		return "default";
	}

//    @RequestMapping(value = "/ajax/configuration", method = RequestMethod.GET)
//    @ResponseBody
//    public Configuration configuration  (Model model) {
//        return new Configuration();
//    }
}
