package com.codesence.bw.service;

import com.codesence.bw.model.Location;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

/**
 * @author Ladislav Bozek
 */
@Service
public class LocationServiceImpl implements LocationService {

    private final Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);

    @Override
    public Location getLocation(String ipAddress) {

        String dataFile = "GeoLite2-City.mmdb";
        return getLocation(ipAddress, dataFile);

    }

    private Location getLocation(String ipAddress, String locationDataFile) {

        logger.info("getLocation(" + ipAddress + ", " + locationDataFile + ")");

        // A File object pointing to your GeoIP2 or GeoLite2 database
        File database = new File(getClass().getClassLoader().getResource(locationDataFile).getFile());

        Location serverLocation = new Location();

        // This creates the DatabaseReader object, which should be reused across lookups.
        DatabaseReader reader = null;
        try {
            reader = new DatabaseReader.Builder(database).build();

            // Replace "city" with the appropriate method for your database, e.g.,
            // "country".
            CityResponse response = reader.city(InetAddress.getByName(ipAddress));
            logger.info("CityResponse: " + response);

            serverLocation.setCountryCode(response.getCountry().getIsoCode());
            serverLocation.setCountryName(response.getCountry().getName());
            serverLocation.setRegion(response.getMostSpecificSubdivision().getIsoCode());
            serverLocation.setRegionName(response.getMostSpecificSubdivision().getName());
            serverLocation.setCity(response.getCity().getName());
            serverLocation.setPostalCode(response.getPostal().getCode());
            serverLocation.setLatitude(
                    String.valueOf(response.getLocation().getLatitude()));
            serverLocation.setLongitude(
                    String.valueOf(response.getLocation().getLongitude()));
//        System.out.println(response.getCountry().getNames().get("zh-CN")); // '美国'

        } catch (GeoIp2Exception e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }

        return serverLocation;
    }
}
