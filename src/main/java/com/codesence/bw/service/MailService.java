package com.codesence.bw.service;

import org.springframework.mail.SimpleMailMessage;

/**
 * This service handles all requests for sending an emails in application.
 *
 * @author Ladislav Bozek
 */
public interface MailService {

	void scheduleEmail(SimpleMailMessage mailMessage);

}
