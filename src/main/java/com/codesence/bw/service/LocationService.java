package com.codesence.bw.service;

import com.codesence.bw.model.Location;

/**
 * @author Ladislav Bozek
 */
public interface LocationService {
    Location getLocation(String ipAddress);
}
