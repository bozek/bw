package com.codesence.bw.service.mailsender;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

/**
 * Inspired by http://www.i-develop.be/blog/2010/10/01/execute-tasks-asynchronously-with-spring-3-0/
 * <p/>
 * TODO implement also async version of this sender as described in provided tutorial
 *
 * @author Ladislav Bozek
 */
@Component(value = "asyncMailSender")
public class AsyncMailSender extends AbstractMailSender {
	private final Logger logger = LoggerFactory.getLogger(AsyncMailSender.class.getName());

	/**
	 * In this class the Spring MailSender is used to send emails.
	 */
	@Resource(name = "mailSender")
	private MailSender mailSender;

	private TaskExecutor taskExecutor;

	@Autowired
	public AsyncMailSender(TaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}

	public void send(SimpleMailMessage simpleMessage) throws MailException {
		logger.debug("Scheduling message for execution: {}", simpleMessage);
		taskExecutor.execute(new AsyncMailTask(simpleMessage));
	}

	private class AsyncMailTask implements Runnable {

		private SimpleMailMessage message;

		private AsyncMailTask(SimpleMailMessage message) {
			this.message = message;
		}

		public void run() {
			logger.debug("Sending message asynchronously: {}", message);
			mailSender.send(message);
		}
	}

}
