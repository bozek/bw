package com.codesence.bw.service.mailsender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

/**
 * This class is responsible for sending emails. Emails can be sent directly from application or by scheduled queue that reads emails from DB. Descendants of
 * this class provides concrete implementation.
 *
 * @author Ladislav Bozek
 */
public abstract class AbstractMailSender implements MailSender {
	private final Logger logger = LoggerFactory.getLogger(AbstractMailSender.class.getName());

	public void send(SimpleMailMessage[] mailMessages) throws MailException {
		if (mailMessages == null) {
			logger.debug("No input specified");

		}

		logger.debug("Number of messages to send: {0}", mailMessages.length);
		for (SimpleMailMessage message : mailMessages) {
			send(message);
		}
	}
}
