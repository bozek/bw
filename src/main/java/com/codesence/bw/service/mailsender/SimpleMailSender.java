package com.codesence.bw.service.mailsender;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

/**
 * Inspired by http://www.i-develop.be/blog/2010/10/01/execute-tasks-asynchronously-with-spring-3-0/
 * <p/>
 * TODO implement also async version of this sender as described in provided tutorial
 *
 * @author Ladislav Bozek
 */
@Component(value = "simpleMailSender")
public class SimpleMailSender extends AbstractMailSender {
	private final Logger logger = LoggerFactory.getLogger(SimpleMailSender.class.getName());

	/**
	 * In this class the Spring MailSender is used to send emails.
	 */
	@Resource(name = "mailSender")
	private MailSender mailSender;

	public void send(SimpleMailMessage simpleMessage) throws MailException {
		logger.debug("Sending message: {}", simpleMessage);
		mailSender.send(simpleMessage);
	}
}
