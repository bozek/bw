package com.codesence.bw.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bozek on 21/09/14.
 */
public class Galleries {
    private List<Gallery> list = new ArrayList<>();

    public Galleries() {
    }

    public List<Gallery> getList() {
        return list;
    }

    public void setList(List<Gallery> list) {
        this.list = list;
    }
}
