package com.codesence.bw.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bozek on 21/09/14.
 */
public class WeatherOptions {

    private List<WeatherOption> list = new ArrayList<>();

    public WeatherOptions() {
    }

    public WeatherOptions(List<WeatherOption> list) {
        this.list = list;
    }

    public List<WeatherOption> getList() {
        return list;
    }

    public void setList(List<WeatherOption> list) {
        this.list = list;
    }

    public void add(WeatherOption weatherOption) {
        this.list.add(weatherOption);
    }

}
