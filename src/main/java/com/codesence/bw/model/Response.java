package com.codesence.bw.model;

/**
 * @author bozek
 */
public class Response {

	private Error error = new Error();

	public Response() {
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}
}
