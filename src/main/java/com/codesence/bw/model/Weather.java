package com.codesence.bw.model;

import java.util.Date;

/**
 * Information about the weather.
 * @author bozek
 */
public class Weather {

    private Date date;
    private int temperature;

    public Weather() {
    }

    public Weather(Date date, int temperature) {
        this.date = date;
        this.temperature = temperature;
    }

    public Date getDate() {
        return date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

}
