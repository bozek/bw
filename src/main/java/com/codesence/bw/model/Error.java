package com.codesence.bw.model;

/**
 * @author bozek 
 */
public class Error {

	private int code = 0;
	private String message = "";

    public Error() {
    }

    public Error(int code) {
        this.code = code;
    }

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
