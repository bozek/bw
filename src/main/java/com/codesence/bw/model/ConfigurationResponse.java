package com.codesence.bw.model;

/**
 * @author bozek
 */
public class ConfigurationResponse extends Response {
    private Location location;
    private Weather weather;
    private User user;
    private WeatherOptions weatherOptions;
    private Organization organization;

    public ConfigurationResponse(Location location, Weather weather, User user, WeatherOptions weatherOptions, Organization organization) {
        this.location = location;
        this.weather = weather;
        this.user = user;
        this.weatherOptions = weatherOptions;
        this.organization = organization;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public WeatherOptions getWeatherOptions() {
        return weatherOptions;
    }

    public void setWeatherOptions(WeatherOptions weatherOptions) {
        this.weatherOptions = weatherOptions;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }
}
