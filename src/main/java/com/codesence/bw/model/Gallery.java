package com.codesence.bw.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bozek on 21/09/14.
 */
public class Gallery {
    private List<String> pictures = new ArrayList<String>();

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }
}
