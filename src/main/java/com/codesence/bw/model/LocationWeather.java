package com.codesence.bw.model;

import java.util.Date;

/**
 * Information about the weather.
 *
 * @author bozek
 */
public class LocationWeather extends Weather {

	private Location location;

	public LocationWeather() {
	}

	public LocationWeather(Date date, int temperature, Location location) {
		super(date, temperature);
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
}
