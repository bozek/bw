package com.codesence.bw.model;

import java.math.BigDecimal;

/**
 * Created by bozek on 21/09/14.
 */
public class WeatherOption {
    private Weather weather;
    private BigDecimal price;

    public WeatherOption() {
    }

    public WeatherOption(Weather weather, BigDecimal price) {
        this.weather = weather;
        this.price = price;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
