package com.codesence.bw.model;

import java.math.BigDecimal;

/**
 * Created by bozek on 21/09/14.
 */
public class Payment {
    private User user;
    private BankAccount bankAccount;

    /**
     * payment identification number
     */
    private long code;

    private BigDecimal price;
}
