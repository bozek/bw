<%@ page language="java" pageEncoding="UTF-8"
	contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>eTimesheet</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8" />

<%-- <link rel="stylesheet" type="text/css"	href="${pageContext.request.contextPath}/assets/bootstrap/2.2.2/css/bootstrap.min.css"	media="screen" /> --%>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/assets/bootswatch-cerulean/3.2.0/css/bootstrap.min.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
< ![endif]-->

<style>
body {
	padding-top: 60px;
	/* move it down a little bit */
}
</style>
</head>
<body>

	<div class="container">
		<div class="row clearfix">
			<div class="col-md-12 column">
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span><span
								class="icon-bar"></span><span class="icon-bar"></span><span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Friend Deal</a>
					</div>

					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown">Menu<strong class="caret"></strong></a>
								<ul class="dropdown-menu">
									<li><a target="_blank" href="${pageContext.request.contextPath}/">Action 1</a></li>
									<li><a target="_blank" href="${pageContext.request.contextPath}/">Action 2</a></li>
								</ul></li>
						</ul>
					</div>

				</nav>
				<div class="jumbotron">
					<h1>Friend Deal</h1>
					<p>Hello world</p>
					<p>
						<a class="btn btn-primary btn-large" target="_blank"
							href="http://www.codesence.com">Learn more about
							CodeSence</a>
					</p>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-md-6 column">
				<h2>Action 1</h2>
				<p>This is action number 1.</p>
				<p>
					<a class="btn" target="_blank" href="${pageContext.request.contextPath}/">Action 1 »</a>
				</p>
			</div>
			<div class="col-md-6 column">
				<h2>Action 2</h2>
				<p>This is action number 2.</p>
				<p>
					<a class="btn" target="_blank" href="${pageContext.request.contextPath}/">Action 2 »</a>
				</p>
			</div>
		</div>
	</div>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/jquery/1.8.2/jquery.min.js"></script>
	<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/bootstrap/2.2.2/js/bootstrap.min.js"></script> --%>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/assets/bootswatch-cerulean/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>