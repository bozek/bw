# README #

### What is this repository for? ###

* Repository for BetterWeather Project
* Version 0.0.1-SNAPSHOT

### How do I get set up? ###

To compile an start application execute following command in the project root folder. 

```
#!java
mvn clean install jetty:run
```


* TODO Summary of set up
* TODO Configuration
* TODO Dependencies
* TODO Database configuration
* TODO How to run tests
* TODO Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact